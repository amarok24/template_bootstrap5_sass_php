# the name of your Sass transpiler executable:
SASS_EXECUTABLE = sass
SCSS_SRC = scss
SCSS_BUILD = assets/css

SASS_WATCH_FLAGS = --no-source-map --watch
SASS_BUILD_FLAGS = --style=compressed


intro:
	@echo "Use one of: make watch, make build, make dev"

# Build SCSS for production
build:
	$(SASS_EXECUTABLE) $(SASS_BUILD_FLAGS) $(SCSS_SRC):$(SCSS_BUILD)

# Start SCSS watch mode
watch:
	$(SASS_EXECUTABLE) $(SASS_WATCH_FLAGS) $(SCSS_SRC):$(SCSS_BUILD)

# Run a local development server
dev:
	php -S localhost:8000


.PHONY = intro build watch dev
