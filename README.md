# Template: Bootstrap 5 / Sass (SCSS) / PHP

An optimized starter template for Bootstrap 5 and Sass (SCSS).

Online demo [here](https://labs.introweb.eu/bootstrap-template/)

- Tested on different desktop browsers and on Google Android and Apple iOS
  devices
- Optimized for accesibility (page readers)
- Accessible via keyboard: use "ESC" to open the navigation, "TAB" to switch
  between sections, "ENTER" for action
- Optimized for fast loading
- [PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)
  results: mobile 94%, desktop 99%
- Basic setup for switching between light/dark mode included (new approach since BS 5.3)

## Requirements
PHP **Composer** is used as dependency manager for [Bootstrap](https://getbootstrap.com/).

A **Make** build tool is used for watch/build/dev (see below), so make sure you have `make` available if you want to take advantage of it. [GNU Make](https://www.gnu.org/software/make/) is included in *all* GNU/Linux distros (but may not be pre-installed).

And of course a **Sass** transpiler is needed to translate SCSS to CSS. Everything is prepared for [Dart Sass](https://github.com/sass/dart-sass/releases), follow this link for easy install instructions. Don't use [NPM Sass](https://www.npmjs.com/package/sass) transpiler, it's slow.

## "Make" build tool options

```sh
make watch
make build
make dev
```

to start Sass in watch mode, transpile SCSS->CSS for production or to start a
local dev server (with PHP's build in -S flag).

## Use as a project

Click the "Use this template" button on the GitHub page to create your own copy
directly on GitHub.

## Clone with Git

Be sure you have Git and Composer installed. Run:

```sh
git clone https://github.com/Amarok24/template_bootstrap5_sass_php.git
```

Then enter the automatically created directory and run:

```sh
composer install
```

to install all dependencies (Bootstrap).

### Older project (Bootstrap 5.1 and NodeJS)

There is also an older version of this project at
[GitHub](https://github.com/Amarok24/template-bootstrap-sass) which uses Bootstrap
5.1 and NPM as package manager.
